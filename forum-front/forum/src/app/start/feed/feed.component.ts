import { Component, OnInit } from '@angular/core';
import { faTrashAlt, faPen, faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmModalComponent } from 'src/app/shared/modals/confirm-modal/confirm-modal.component';
import { Post } from 'src/app/shared/models/post';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { PostService } from 'src/app/shared/services/post.service';
import { UserService } from 'src/app/shared/services/user.service';
import { environment } from 'src/environments/environment';
import { PostModalComponent } from '../post-modal/post-modal.component';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss']
})
export class FeedComponent implements OnInit {

  postList: Post[];
  userLookUp: Object;
  userID: string; 

  noPosts = true;
  feedMessage = 'No posts yet.';
  goodReadsUrl = environment.config.goodReadsUrl;

  faTrashAlt = faTrashAlt;
  faPen = faPen;
  faExtLink = faExternalLinkAlt;

  constructor(
    private postService: PostService,
    private userService: UserService,
    private localStorageService: LocalStorageService,
    private modal: NgbModal
  ) {
    this.postList = [];
  }

  ngOnInit(): void {
    this.userID = this.localStorageService.getUserData()._id;

    this.userService.getUsersDictionary().subscribe(
      (res) => {
        this.userLookUp = res;
        this.postService.getPosts().subscribe(
          (posts) => {
            if (posts && posts.length > 0) {
              this.noPosts = false;
              this.postList = posts;
              console.log(posts); 
            } 
          }
        );
      },
        (err) => {
          this.noPosts = true;
          this.feedMessage = 'Something went wrong...'
        }
    )
  }

  onEdit(index: number) {
    const modal = this.modal.open(PostModalComponent, { size: 'lg', centered: true });
    modal.componentInstance.editMode = true;
    modal.componentInstance.setData(this.postList[index]);

    modal.result
      .then(res => {
        this.postList[index] = res;
      })
      .catch(reason => {});
  }

  onDelete(index: number) {
    const modal = this.modal.open(ConfirmModalComponent, { size: 'sm' });
    modal.componentInstance.messageTitle = 'Delete';
    
    modal.result
      .then(answer => {
        this.postService.deletePost(this.postList[index]._id).subscribe(
          (res) => {
            this.ngOnInit();
          }
        )
      })
      .catch(reason => {});
  }

  isOwnPost(post: Post): boolean {
    return post.userId == this.userID;
  }
}
