import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PostModalComponent } from '../post-modal/post-modal.component';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {
  @Output() newPost = new EventEmitter<string>();
  
  constructor(
    private modal: NgbModal,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  onAdd() {
    const modal = this.modal.open(PostModalComponent, { size: 'lg', centered: true });
    
    modal.result
      .then(res => {
        console.log(res);
        this.newPost.next('Post posted');
      })
      .catch(reason => {});
  }
}
