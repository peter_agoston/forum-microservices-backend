import { Component, OnInit, ViewChild } from '@angular/core';
import { FeedComponent } from '../feed/feed.component';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  @ViewChild(FeedComponent) feed: FeedComponent;

  constructor() { }

  ngOnInit(): void {
  }

  onNewPost() {
    this.feed.ngOnInit();
  }
}
