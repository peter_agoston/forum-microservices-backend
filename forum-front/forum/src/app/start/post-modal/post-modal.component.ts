import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Book } from 'src/app/shared/models/book';
import { Post } from 'src/app/shared/models/post';
import { BookService } from 'src/app/shared/services/book.service';
import { PostService } from 'src/app/shared/services/post.service';

@Component({
  selector: 'app-post-modal',
  templateUrl: './post-modal.component.html',
  styleUrls: ['./post-modal.component.scss']
})
export class PostModalComponent implements OnInit {

  public postForm: FormGroup;
  public editMode = false;

  waitingForSearchResponse = false;

  private book: Book;
  private post: Post;

  constructor(
    private fb: FormBuilder,
    private bookService: BookService,
    private postService: PostService,
    public activeModal: NgbActiveModal
  ) {
    this.book = new Book();
    this.post = new Post();
  }

  public setData(post: Post) {
    if (post) {
      this.post = post;    
      if (post.book) {
        this.book = this.post.book;
      }
    }
  }

  ngOnInit(): void {
    this.postForm = this.fb.group({
      quote: [this.post.quote],
      content: [this.post.content],
      isbn: [this.book.isbn, [Validators.pattern('([0-9]{10}|[0-9]{13})')]],
      author: [this.book.author],
      title: [this.book.title]
    }, {
      validators: [this.noTextValidation, this.noBookInfo]
    });

    this.postForm.get('isbn').valueChanges.subscribe(val => {
      if (this.postForm.get('isbn').valid && this.postForm.get('isbn').value) {
        this.waitingForSearchResponse = true;
        
        this.bookService.searchForBook(val).subscribe((book: Book) => {
          this.book = book;
          this.postForm.get('title').setValue(book.title);
          this.postForm.get('author').setValue(book.author);
          this.waitingForSearchResponse = false;
        },
        err => {
          this.waitingForSearchResponse = false;
        })
      }
    });
  }

  noTextValidation(form: FormGroup) {
    const valid = (form.get('content').value || form.get('quote').value) != ''; 
    return valid ? null : {noText: true};
  }

  noBookInfo(form: FormGroup) {
    const isAuthor = form.get('author').value != '';
    const isTitle = form.get('title').value != ''; 
    const valid =  (isAuthor && isTitle) || (!isAuthor && !isTitle)
    return valid ? null : {noBookInfo: true};  
  }

  onSubmit() {
    if (this.postForm.valid) {
      if (this.postForm.get('title').value) {
        this.book.author = this.postForm.get('author').value;
        this.book.title = this.postForm.get('title').value;

        this.bookService.saveBook(this.book).subscribe(
          (bookRes: Book) => {
            this.savePost(bookRes._id);
          },
          err => {
            console.error(err);
          }
        )
      } else {
        this.savePost();
      }
    }
  }

  savePost(bookId?: string) {
    this.post.content = this.postForm.get('content').value;
    this.post.quote = this.postForm.get('quote').value;
    this.post.bookId = bookId;

    if (this.editMode) {
      this.updatePost(bookId);
    } else {
      this.addPost(bookId);
    }
  }

  addPost(bookId?: string) {
    this.postService.savePost(this.post).subscribe(
      (postRes: Post) => {
        console.log(postRes);
        this.activeModal.close(postRes);
      },
      err => console.error(err)
    )
  }

  updatePost(bookId?: string) {
    this.postService.updatePost(this.post._id, this.post).subscribe(
      (postRes: Post) => {
        console.log(postRes);
        this.activeModal.close(postRes);
      },
      err => console.error(err)
    )
  }
  
}
