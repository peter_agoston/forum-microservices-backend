import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout/layout.component';
import { FeedComponent } from './feed/feed.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { StartRoutingModule } from './start-routing.module';
import { SharedModule } from '../shared/shared.module';
import { PostModalComponent } from './post-modal/post-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TimeagoModule } from 'ngx-timeago';

@NgModule({
  declarations: [LayoutComponent, FeedComponent, SideBarComponent, PostModalComponent],
  imports: [
    CommonModule,
    StartRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    FontAwesomeModule,
    TimeagoModule.forRoot()
  ]
})
export class StartModule { }
