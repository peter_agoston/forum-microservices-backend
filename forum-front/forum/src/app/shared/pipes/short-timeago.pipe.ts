import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'shortenTimeago'})
export class ShortTimeagoPipe implements PipeTransform {
    transform(value: string): string {
        const splitText = value.split(' ') 
        return splitText[0] + ' ' + splitText[1][0];
    }
}
