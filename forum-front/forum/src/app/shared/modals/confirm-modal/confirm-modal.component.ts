import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss']
})
export class ConfirmModalComponent implements OnInit {

  public messageTitle = '';
  public messageBody = 'Are you sure?';
  public buttonText = 'Yes';

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }
}

