import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError, map } from 'rxjs/operators';
import { User } from '../models/user';
import { ErrorService } from './error.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  userUrl: string;
  authUrl: string;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    })
  };

  constructor(
    private http: HttpClient,
    private errorService: ErrorService
  ) {
    this.userUrl = environment.config.apiUrl + 'user';
    this.authUrl = environment.config.apiUrl + 'auth/login';
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.userUrl)
    .pipe(
      retry(1),
      catchError(this.errorService.errorHandler)
    );
  }

  getUser(userId: string): Observable<User> {
      return this.http.get<User>(this.userUrl + '/' + userId)
      .pipe(
        retry(1),
        catchError(this.errorService.errorHandler)
      );
  }

  authUser(user: User): Observable<User> {
    return this.http.post<User>(this.authUrl, JSON.stringify(user), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorService.errorHandler)
    );
  }

  saveUser(user: User): Observable<User> {
      return this.http.post<User>(this.userUrl, JSON.stringify(user), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorService.errorHandler)
      );
  }

  updateUser(userId: string, user: User): Observable<User> {
      return this.http.put<User>(this.userUrl + '/' + userId, JSON.stringify(user), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorService.errorHandler)
      );
  }

  deleteUser(userId: string): Observable<User> {
      return this.http.delete<User>(this.userUrl + '/' + userId)
      .pipe(
        retry(1),
        catchError(this.errorService.errorHandler)
      );
  }

  getUsersDictionary(): Observable<any> {
    return this.http.get<User[]>(this.userUrl)
    .pipe(
      map((users: User[]) => {
        const dict = [];

        users.forEach((user: User) => {
          dict[user._id] = user;
        });

        return dict;
      }),
      retry(1),
      catchError(this.errorService.errorHandler)
    )
  }
}
