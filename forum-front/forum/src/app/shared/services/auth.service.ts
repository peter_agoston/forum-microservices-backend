import { LocalStorageService } from './local-storage.service';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../models/user';
import { LoginResponse } from '../models/login-response';
import { NotificationService } from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private localStorageService: LocalStorageService,
    private router: Router,
    private notificationService: NotificationService
  ) { }

  public logOut() {
    this.router.navigateByUrl('/auth');
    this.notificationService.unsubscribe().toPromise().finally(() => {
      this.localStorageService.clear();
    })
  }

  public saveLogIn(res: LoginResponse) {
    this.localStorageService.setToken(res.accessToken);
    const user = new User();
    user._id = res.userID;
    user.username = res.username;
    this.localStorageService.setUserData(user);

    this.notificationService.subscribeToPush();
  }

  public isAuthenticated(): boolean {
    return (this.localStorageService.getUserData() !== null);
  }

  public getToken() {
    return this.localStorageService.getToken();
  }
}
