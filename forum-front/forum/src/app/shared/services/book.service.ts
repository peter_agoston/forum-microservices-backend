import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Book } from '../models/book';
import { ErrorService } from './error.service';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  bookUrl: string;
  
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    })
  };

  constructor(
    private http: HttpClient,
    private errorService: ErrorService
  ) {
    this.bookUrl = environment.config.apiUrl + 'book';
  }

  getBook(bookId: string): Observable<Book> {
      return this.http.get<Book>(this.bookUrl + '/' + bookId)
      .pipe(
        retry(1),
        catchError(this.errorService.errorHandler)
      );
  }

  searchForBook(isbn: string): Observable<Book> {
    return this.http.get<Book>(this.bookUrl + '/search/' + isbn)
    .pipe(
      retry(1),
      catchError(this.errorService.errorHandler)
    );
  }

  saveBook(book: Book): Observable<Book> {
    return this.http.post<Book>(this.bookUrl, JSON.stringify(book), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorService.errorHandler)
    );
  }
}
