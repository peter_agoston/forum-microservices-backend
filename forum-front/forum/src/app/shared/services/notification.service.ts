import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SwPush } from '@angular/service-worker';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Entity } from '../models/entity';
import { ErrorService } from './error.service';
import { LocalStorageService } from './local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  notifyUrl: string;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    })
  };
  
  constructor(
    private swPush: SwPush,
    private http: HttpClient,
    private errorService: ErrorService,
    private localStorageService: LocalStorageService
  ) { 
    this.notifyUrl = `${environment.config.apiUrl}notify/subscribe`;
  }

  async subscribeToPush() {
    try {
      const sub = await this.swPush.requestSubscription({
        serverPublicKey: environment.config.serverVapidKey,
      });
      
      this.http.post(this.notifyUrl, JSON.stringify(sub), this.httpOptions).toPromise()
        .then((response: Entity) => {
          this.localStorageService.setPushSubscriptionID(response._id);
        })
    } catch (err) {
      console.error('Could not subscribe due to:', err);
    }
  }

  emitMessage(): Observable<object> {
    return this.swPush.messages;
  }
  
  unsubscribe(): Observable<object> {
    const subID = this.localStorageService.getPushSubscriptionID();

    
    this.swPush.unsubscribe().catch(err => {
      console.error(err);
    });
    console.log('Unsubscribe...');

    if (subID) {
      return this.http.delete<Entity>(this.notifyUrl + '/' + subID)
        .pipe(
          retry(1),
          catchError(this.errorService.errorHandler)
        );
    } else {
      throw new Error('No subscription found.')
    }
  }
}
