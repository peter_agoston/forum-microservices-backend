import { Injectable } from '@angular/core';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  userDataKey = 'userdata';
  tokenKey = 'token';
  userLookUpKey= 'userLookUp';
  pushNotificationID = 'pushNotificationID';

  constructor() { }

  setUserData(user: User) {
    localStorage.setItem(this.userDataKey, JSON.stringify(user));
  }

  getUserData() {
    return JSON.parse(localStorage.getItem(this.userDataKey));
  }

  setToken(token: string) {
    localStorage.setItem(this.tokenKey, token);
  }

  getToken() {
    return localStorage.getItem(this.tokenKey);
  }

  setPushSubscriptionID(id: string) {
    localStorage.setItem(this.pushNotificationID, id);
  }

  getPushSubscriptionID() {
    return localStorage.getItem(this.pushNotificationID);
  }

  clear() {
    localStorage.clear();
  }
}
