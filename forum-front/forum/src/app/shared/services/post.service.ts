import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Post } from '../models/post';
import { ErrorService } from './error.service';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  postUrl: string;
  
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    })
  };

  constructor(
    private http: HttpClient,
    private errorService: ErrorService
  ) {
    this.postUrl = environment.config.apiUrl + 'post';
  }

  getPost(postId: string): Observable<Post> {
      return this.http.get<Post>(this.postUrl + '/' + postId)
      .pipe(
        retry(1),
        catchError(this.errorService.errorHandler)
      );
  }

  getPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(this.postUrl)
    .pipe(
      retry(1),
      catchError(this.errorService.errorHandler)
    );
  }

  savePost(post: Post): Observable<Post> {
    return this.http.post<Post>(this.postUrl, JSON.stringify(post), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorService.errorHandler)
    );
  }

  updatePost(postId: string, post: Post): Observable<Post> {
    return this.http.put<Post>(this.postUrl + '/' + postId, JSON.stringify(post), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorService.errorHandler)
    );
  }

  deletePost(postId: string): Observable<Post> {
    return this.http.delete<Post>(this.postUrl + '/' + postId)
    .pipe(
      retry(1),
      catchError(this.errorService.errorHandler)
    );
  }
}
