import { Entity } from './entity';

export class Book extends Entity {
    title: string;
    isbn: string;
    author: string;
    goodReadsId: string;
}