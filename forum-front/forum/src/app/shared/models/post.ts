import { Book } from './book';
import { Entity } from './entity';

export class Post extends Entity {
    userId: string;
    content?: string;
    quote?: string; 
    bookId?: string;
    book?: Book;
    date?: string;
}