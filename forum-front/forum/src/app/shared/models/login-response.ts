export class LoginResponse {
    accessToken: string
    userID: string
    username: string
}