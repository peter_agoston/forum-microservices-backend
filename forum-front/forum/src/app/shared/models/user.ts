import { Entity } from './entity';

export class User extends Entity {
  username: string;
  password: string;
  role: string;
  email: string;
  token?: string;
}
