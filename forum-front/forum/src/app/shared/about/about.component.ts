import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  title = '4UM';
  text = 'This is a prototype version of a blog/social platform website.';
  text2 = 'At this stage of the project the website is not public, access is restricted to a select group of personnel.';
  footer = 'Developed by Peter Agoston, 2020.';

  constructor() { }

  ngOnInit() {
  }

}
