import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { UserService } from './services/user.service';
import { LocalStorageService } from './services/local-storage.service';
import { AuthService } from './services/auth.service';
import { AuthGuardService } from './services/auth-guard.service';
import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './interceptors/token-interceptor';
import { BookService } from './services/book.service';
import { ConfirmModalComponent } from './modals/confirm-modal/confirm-modal.component';
import { NotificationService } from './services/notification.service';
import { ServiceWorkerModule } from '@angular/service-worker';
import { ShortTimeagoPipe } from './pipes/short-timeago.pipe';

@NgModule({
  declarations: [AboutComponent, ContactComponent, ConfirmModalComponent, ShortTimeagoPipe],
  imports: [
    CommonModule,
    HttpClientModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: true })
  ],
  exports: [
    AboutComponent, 
    ContactComponent,
    ShortTimeagoPipe
  ],
  providers: [
    UserService,
    BookService,
    LocalStorageService,
    AuthService,
    NotificationService,
    AuthGuardService,
    {
      provide : HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi   : true,
    }
  ]
})
export class SharedModule { }
