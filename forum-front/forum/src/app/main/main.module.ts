import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { NavComponent } from './nav/nav.component';
import { MainComponent } from './main/main.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SharedModule } from '../shared/shared.module';
import { TimeagoModule } from 'ngx-timeago';


@NgModule({
  declarations: [NavComponent, MainComponent],
  imports: [
    CommonModule,
    MainRoutingModule,
    NgbModule,
    FontAwesomeModule,
    SharedModule,
    TimeagoModule.forRoot()
  ]
})
export class MainModule { }
