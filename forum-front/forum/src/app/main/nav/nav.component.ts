import { AuthService } from './../../shared/services/auth.service';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { SwPush } from '@angular/service-worker';
import { faBell, faCaretRight } from '@fortawesome/free-solid-svg-icons';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  @Output() refreshRequest = new EventEmitter();

  timestamp: Date;
  faBell = faBell;
  faCaretRight = faCaretRight;
  username = 'User';

  //notifications = [{ title: 'New Post by Agoston!', timestamp: new Date() },  {    title: 'New Post by Zalan!', timestamp: new Date() }];
  notifications = [];

  constructor(
    private authService: AuthService,
    private swPush: SwPush,
    private localStorageService: LocalStorageService,
    private router: Router
    ) { 
    this.swPush.messages.subscribe(
      msg => {
        console.log("New NOTIFICATION");
        this.notifications.push(msg['notification'])
        console.log(msg);
      }
    );
  }

  ngOnInit() {
    this.username = this.localStorageService.getUserData().username;
    this.timestamp = new Date();
  }

  onSignOut() {
    this.authService.logOut();
  }

  toggled(event) {
    if (!event) {
      this.notifications = [];
    }
  }

  notificationClicked() {
    if (this.router.url == '/') {
      this.refreshRequest.emit(); 
    }
  }
}
