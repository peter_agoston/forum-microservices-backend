import { Component, OnInit, ViewChild } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { FeedComponent } from 'src/app/start/feed/feed.component';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  @ViewChild(RouterOutlet) outlet;

  constructor(  ) {}

  ngOnInit() {  }

  refreshFeed() {
    if (this.outlet && this.outlet.component && this.outlet.component.feed 
            && this.outlet.component.feed instanceof FeedComponent) {
      (this.outlet.component.feed as FeedComponent).ngOnInit();
    }
  }
}
