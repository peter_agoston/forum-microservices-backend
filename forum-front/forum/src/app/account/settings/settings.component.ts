import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  success = false;
  failure = false;
  failureMessage = '';
  passwordMismatch = false;

  public pwForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    // private auth: AuthService,
    // private localStorageService: LocalStorageService
  ) { }

  ngOnInit() {
    this.pwForm = this.fb.group({
      currentPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    });
  }

  onPasswordChange() {
    this.passwordMismatch = false;
  }

  savePassword() {
    /*this.success = false;
    this.failure = false;

    const oldPassword = this.pwForm.value.currentPassword;
    const newPassword = this.pwForm.value.newPassword;
    const confirmPassword = this.pwForm.value.confirmPassword;

    if (newPassword === confirmPassword) {
      const userID = this.localStorageService.getLoggedInUser()._id;

      this.auth.changePassword(userID, oldPassword, newPassword).subscribe(
        res => {
          this.success = true;
        },
        err => {
          console.log(err);
          this.failure = true;
          if (err.status === 401) {
            this.failureMessage = 'Current password is not correct.';
          } else {
            this.failureMessage = 'Something went wrong.';
          }
          this.pwForm.setValue({
            currentPassword: '',
            newPassword: '',
            confirmPassword: ''
          });
        }
      );
    } else {
      this.passwordMismatch = true;
    }*/
  }
}
