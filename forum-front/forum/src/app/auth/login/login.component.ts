import { AuthService } from './../../shared/services/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/services/user.service';
import { Router } from '@angular/router';
import { User } from 'src/app/shared/models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  salutaions = [
    'Hello there!',
    'Ciao!',
   // 'Kézcsók!',
   // 'Buongiorno!',
    'Salut!'
  ];
  actSalutation = '';

  public loginForm: FormGroup;
  showErr = false;

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private auth: AuthService,
    private router: Router
  ) {  }

  ngOnInit() {
    const randNum = Math.floor(Math.random() * this.salutaions.length);
    this.actSalutation = this.salutaions[randNum];

    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });
  }

  submit() {
    const user = new User();
    user.email = this.loginForm.value.email;
    user.password = this.loginForm.value.password;

    this.userService.authUser(user).subscribe((res: any) => {
      this.auth.saveLogIn(res);
      this.router.navigateByUrl('/');
    },
    err => {
      console.error(err);
      this.showErr = true;
    });
  }
}
