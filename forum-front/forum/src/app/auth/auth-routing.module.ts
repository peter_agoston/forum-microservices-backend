import { ContactComponent } from './../shared/contact/contact.component';
import { AboutComponent } from './../shared/about/about.component';
import { LoginComponent } from './login/login.component';
import { AuthComponent } from './auth/auth.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [{
  path: 'auth',
  component: AuthComponent,
  children: [
    { path: '', component: LoginComponent },
    { path: 'about', component: AboutComponent},
    { path: 'contact', component: ContactComponent}
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
