import { Module } from '@nestjs/common';
import { BookController } from './book.controller';
import { LibraryService } from './library.service';
import { BookService } from './book.service';

@Module({
  controllers: [BookController],
  providers: [LibraryService, BookService]
})
export class BookModule {}
