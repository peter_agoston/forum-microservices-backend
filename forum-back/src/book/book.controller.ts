import { CreateBookDto } from '../shared/dtos/create-book.dto';
import { LibraryService } from './library.service';
import { Body, Controller, Get, HttpStatus, NotFoundException, Param, Post, Res, UseGuards } from '@nestjs/common';
import { Response } from 'express';
import { BookService } from './book.service';
import {
    ApiBearerAuth,
    ApiOperation,
    ApiResponse,
    ApiTags,
  } from '@nestjs/swagger';
import { Book } from '../shared/entities/book';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@UseGuards(JwtAuthGuard)
@ApiTags('book')
@ApiBearerAuth()
@Controller('book')
export class BookController {
    constructor(
        private libraryService: LibraryService,
        private bookService: BookService
    ) {}

    @Post()
    @ApiOperation({ summary: 'Create book' })
    @ApiResponse({ status: HttpStatus.CREATED, description: 'Created.' })
    @ApiResponse({ status: HttpStatus.ACCEPTED, description: 'Document already exists.' }) 
    async create(@Res() res: Response, @Body() dto: CreateBookDto) {
        
        this.bookService.create(dto).then(book => {
            // this.userWsGateway.broadcast({ event: 'created', payload: item });
            return res.status(HttpStatus.CREATED).send(book);
        })
        .catch(error => {
            return res.status(HttpStatus.BAD_REQUEST).send(JSON.stringify(error));
        })
            
    }

    @Get()
    @ApiOperation({ summary: 'List all books' })
    @ApiResponse({ status: HttpStatus.OK, type: [Book] })
    @ApiResponse({ status: HttpStatus.UNAUTHORIZED, description: 'Admin-only access.' })
    async findAll(): Promise<Book[]> {
        return this.bookService.findAll();
    }

    
    @Get('search/:isbn')
    @ApiOperation({ summary: 'Find book by ISBN' })
    @ApiResponse({ status: HttpStatus.OK, type: Book })
    @ApiResponse({ status: HttpStatus.NOT_FOUND, description: 'Not found.' })
    async findBook(@Param('isbn') isbn: string): Promise<Book> {
        return this.libraryService.findBookByIsbn(isbn)
            .catch(err => {
                console.log(err);
                throw new NotFoundException();
            })
    }
}
