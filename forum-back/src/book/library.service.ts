import { Book } from '../shared/entities/book';
import { Injectable } from '@nestjs/common';
import { Transport, ClientProxyFactory, ClientProxy } from '@nestjs/microservices';
import { library_host } from 'src/config';

@Injectable()
export class LibraryService {
    private libraryClient: ClientProxy;

    constructor(  ) {
        this.libraryClient = ClientProxyFactory.create({
        transport: Transport.TCP,
        options: {
            host: library_host,
            port: 8885
        }
        });
    }
  
    async findBookByIsbn(isbn: string): Promise<Book> {   
        return this.libraryClient.send('getBookInfo', isbn).toPromise();
    }
}
