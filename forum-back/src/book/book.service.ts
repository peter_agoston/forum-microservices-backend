import { book_host } from './../config';
import { Injectable } from '@nestjs/common';
import { ClientProxy, ClientProxyFactory, Transport } from '@nestjs/microservices';
import { Book } from '../shared/entities/book';
import { CreateBookDto } from '../shared/dtos/create-book.dto';

@Injectable()
export class BookService {
  private bookClient: ClientProxy;

  constructor(  ) {
    this.bookClient = ClientProxyFactory.create({
      transport: Transport.TCP,
      options: {
          host: book_host,
          port: 8895
      }
    });
  }

  async create(createBookDto: CreateBookDto): Promise<Book> {
    return this.bookClient.send('create', createBookDto).toPromise();
  }

  async findAll(): Promise<Book[]> { // TO-DO data to represent limit 
    return this.bookClient.send('findAll', 1).toPromise();
  }
}
