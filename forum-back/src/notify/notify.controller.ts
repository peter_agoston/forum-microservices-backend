import { Body, Controller, HttpStatus, Post, Res, Request, UseGuards, Delete, Param } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { SubscriptionDTO } from 'src/shared/dtos/subscription-dto';
import { NotifyService } from './notify.service';

@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@ApiTags('notify')
@Controller('notify')
export class NotifyController {

    constructor (
        private notifyService: NotifyService
    ) {}

    @Post('subscribe')
    @ApiOperation({ summary: 'Subscription for web push notifications.' })
    async subscribe(@Request() req, @Res() res: Response, @Body() subscription: SubscriptionDTO) {
        subscription.userId = req.user._id

        this.notifyService.register(subscription)
            .then(resp => {
                return res.status(HttpStatus.CREATED).send(resp);
            })
            .catch(error => {
                return res.status(HttpStatus.BAD_REQUEST).send(JSON.stringify(error));
            })
    }

    @Delete('subscribe/:id')
    @ApiOperation({ summary: 'Unsubscription from web push notifications.' })
    async unsubscribe(@Param('id') id: string, @Res() res: Response) {
        return this.notifyService.unregister(id).then(sub => {
            return res.status(HttpStatus.OK).send(sub);
        })
        .catch(error => {
            return res.status(HttpStatus.NOT_FOUND).send(JSON.stringify(error));
        })
    }
}
