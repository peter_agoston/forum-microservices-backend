import { Injectable } from '@nestjs/common';
import { ClientProxy, ClientProxyFactory, Transport } from '@nestjs/microservices';
import { noty_host } from 'src/config';
import { SubscriptionDTO } from 'src/shared/dtos/subscription-dto';

@Injectable()
export class NotifyService {
    private notyClient: ClientProxy;

    constructor() {
        this.notyClient = ClientProxyFactory.create({
          transport: Transport.TCP,
          options: {
              host: noty_host,
              port: 9000
          }
        });
    }

  async register(subscription: SubscriptionDTO): Promise<any> {
    return this.notyClient.send('register', subscription).toPromise();
  }

  async unregister(id: string): Promise<any> {
    return this.notyClient.send('unregister', id).toPromise();
  }
}
