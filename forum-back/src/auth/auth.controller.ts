import { Controller, Post, UseGuards, Request, HttpStatus } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { LoginResponse } from '../shared/entities/login-response';
import { LocalAuthGuard } from './local-auth.guard';

@ApiTags('auth')
@Controller()
export class AuthController {
  constructor(private readonly authService: AuthService) {
  }

  @UseGuards(LocalAuthGuard)
  @Post('auth/login')
  @ApiOperation({ summary: 'Log-in user.' })
  @ApiResponse({ status: HttpStatus.OK, type: LoginResponse })
  @ApiResponse({ status: HttpStatus.UNAUTHORIZED, description: 'Log-in failed.' })
  async login(@Request() req): Promise<LoginResponse> {
    return this.authService.login(req.user);
  }
}
