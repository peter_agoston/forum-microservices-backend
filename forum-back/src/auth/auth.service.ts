import { Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { LoginResponse } from '../shared/entities/login-response';
import { User } from 'src/shared/entities/user';

// TO-DO: change any type to User

@Injectable()
export class AuthService {

    constructor(
        private userService: UserService,
        private jwtService: JwtService
    ) {}

    async validateUser(email: string, pass: string): Promise<any> {
        const user = await this.userService.findByEmail(email);
        if (user) {
            const isPasswordMatching = await bcrypt.compare(pass, user.password);

            if (isPasswordMatching) {
                return user;
            }
        }
        return null;
    }

    async login(user: User): Promise<LoginResponse> {
        const payload = { email: user.email, sub: user._id };
        return new LoginResponse(
                this.jwtService.sign(payload),
                user._id,
                user.name
        );
    }

    async getUserInfo(id: string): Promise<any> {
        return this.userService.findById(id);
    }
}
