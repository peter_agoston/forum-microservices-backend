import { JwtAuthGuard } from './../auth/jwt-auth.guard';
import { CreateUserDto } from '../shared/dtos/create-user.dto';
import { UserService } from './user.service';
import { Body, Controller, Get, HttpStatus, Post, Res, UseGuards } from '@nestjs/common';
import { User } from '../shared/entities/user';
import { Response } from 'express';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

@ApiTags('user')
@Controller('user')
export class UserController {
    constructor(private readonly userService: UserService) {}

    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth()
    @ApiOperation({ summary: 'List all users.' })
    @ApiResponse({ status: HttpStatus.OK, type: [User] })
    @ApiResponse({ status: HttpStatus.UNAUTHORIZED, description: 'Admin-only access.' })
    @Get()
    async findAll(): Promise<User[]> {
        return this.userService.findAll();
    }

    @Post()
    @ApiOperation({ summary: 'Register user.' })
    @ApiResponse({ status: HttpStatus.CREATED, description: 'Created.' })
    @ApiResponse({ status: HttpStatus.BAD_REQUEST, description: 'Invalid data.' })
    async create(@Res() res: Response, @Body() dto: CreateUserDto) {
        
        this.userService.create(dto).then(user => {
            // this.userWsGateway.broadcast({ event: 'created', payload: item });
            user.password = undefined;
            return res.status(HttpStatus.CREATED).send(user)
        })
        .catch(error => {
            return res.status(HttpStatus.BAD_REQUEST).send(JSON.stringify(error))
        })
            
    }
}
