import { Injectable } from '@nestjs/common';
import { Transport, ClientProxyFactory, ClientProxy } from '@nestjs/microservices';
import { User } from '../shared/entities/user';
import { CreateUserDto } from '../shared/dtos/create-user.dto';
import { user_host } from '../config';

@Injectable()
export class UserService { 
  private userClient: ClientProxy;

  constructor(  ) {
    this.userClient = ClientProxyFactory.create({
      transport: Transport.TCP,
      options: {
          host: user_host,
          port: 8875
      }
    });
  }

  async create(createUserDto: CreateUserDto): Promise<User> {
    return this.userClient.send('create', createUserDto).toPromise();
  }

  async findAll(): Promise<User[]> { // TO-DO data to represent limit 
    return this.userClient.send('findAll', 1).toPromise();
  }

  async findByEmail(email: string): Promise<User | undefined> {
      return this.userClient.send('findByEmail', email).toPromise();
  }

  async findById(id: string): Promise<User | undefined> {
    return this.userClient.send('findById', id).toPromise();
  }
}
