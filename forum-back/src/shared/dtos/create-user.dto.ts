import { ApiProperty } from "@nestjs/swagger";
import { IsString } from "class-validator";

export class CreateUserDto {
    @IsString()
    @ApiProperty({ description: 'Name.'})
    name: string;
    
    @IsString()
    @ApiProperty({ description: 'Email. (unique)'})
    email: string;

    @IsString()
    @ApiProperty({ description: 'Password.'})
    password: string;
}
