import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class CreatePostDto {
    @IsString()
    userId: string;
  
    @IsString()
    userName: string;

    @IsString()
    @IsOptional()
    @ApiPropertyOptional({ description: 'Text of the post.' })
    content: string;
  
    @IsString()
    @IsOptional()
    @ApiPropertyOptional({ description: 'Text of quote.' })
    quote: string; 
  
    @IsString()
    @IsOptional()
    @ApiPropertyOptional({ description: 'ID of the referenced book.' })
    bookId: string;
}