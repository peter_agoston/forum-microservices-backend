import { CreatePostDto } from './create-post.dto';

export class UpdatePostDto extends CreatePostDto {
    _id: string;

    constructor(dto: CreatePostDto) {
        super()
        Object.assign(this, dto);
    }
}
