import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class CreateBookDto {
    @IsString()  
    @ApiProperty({ description: 'Title.' })
    title: string;

    @IsString()
    @ApiProperty({ description: 'ISBN of the book (13-digit recommended).' })
    isbn: string;
    
    @IsString()
    @ApiProperty({ description: 'Name of author (or authors separated with comma).' })
    author: string;
    
    @IsString()
    @IsOptional()
    @ApiPropertyOptional({ description: 'GoodReads ID of the book.' })
    goodReadsId: string;
}
