import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class SubscriptionDTO {
    @IsString()  
    @ApiProperty({ description: 'Absolute URL exposed by the push service.' })
    endpoint: string;

    @IsString()
    @ApiProperty({ description: 'Pair of p256dh and auth keys.' })
    keys: Object;
    
    @IsString()
    userId: string;
}
