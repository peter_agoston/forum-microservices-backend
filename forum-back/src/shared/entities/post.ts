import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { Entity } from "src/shared/entities/entity";
import { CreatePostDto } from "../dtos/create-post.dto";
import { Book } from "./book";

export class Post extends Entity {
    @ApiProperty({ description: 'User ID.' })  
    userId: string;
  
    @ApiPropertyOptional({ description: 'Text of the post.' })
    content: string;
  
    @ApiPropertyOptional({ description: 'Text of quote.' })
    quote: string; 
  
    @ApiPropertyOptional({ description: 'Referenced book.' })
    book: Book;

    @ApiProperty({ description: 'Last modified (read-only).'})
    date: string;

    constructor(dto: CreatePostDto) {
        super();
        Object.assign(this, dto);
    }
}
