import { ApiProperty } from "@nestjs/swagger";
import { Entity } from "src/shared/entities/entity";

export class User extends Entity {
    @ApiProperty({ description: 'Name.'})
    name: string;
    
    @ApiProperty({ description: 'Email. (unique)'})
    email: string;

    @ApiProperty({ description: 'Password.'})
    password: string;
}
