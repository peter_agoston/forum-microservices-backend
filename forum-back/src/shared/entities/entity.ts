import { ApiProperty } from "@nestjs/swagger";

export class Entity {
    @ApiProperty()
    _id: string;
}