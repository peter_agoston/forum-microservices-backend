import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Entity } from 'src/shared/entities/entity';

export class Book extends Entity {
    @ApiProperty({ description: 'Title.' })
    title: string;

    @ApiProperty({ description: 'ISBN of the book (13-digit recommended).' })
    isbn: string;
    
    @ApiProperty({ description: 'Name of author (or authors separated with comma).' })
    author: string;
    
    @ApiPropertyOptional({ description: 'GoodReads ID of the book.' })
    goodReadsId: string;
}
