import { ApiProperty } from "@nestjs/swagger";

export class LoginResponse {
    @ApiProperty()
    accessToken: string

    @ApiProperty()
    userID: string

    @ApiProperty()
    username: string

    constructor(token: string, id: string, username?: string) {
        this.accessToken = token;
        this.userID = id;
        this.username = username;
    }
}