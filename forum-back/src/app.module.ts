import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { BookModule } from './book/book.module';
import { PostModule } from './post/post.module';
import { NotifyModule } from './notify/notify.module';

@Module({
  imports: [
    UserModule,
    AuthModule,
    BookModule,
    PostModule,
    NotifyModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
