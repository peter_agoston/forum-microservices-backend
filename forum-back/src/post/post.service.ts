import { Injectable } from '@nestjs/common';
import { ClientProxy, ClientProxyFactory, Transport } from '@nestjs/microservices';
import { post_host } from 'src/config';
import { User } from 'src/shared/entities/user';
import { CreatePostDto } from '../shared/dtos/create-post.dto';
import { UpdatePostDto } from '../shared/dtos/update-post.dto';
import { Post } from '../shared/entities/post';

@Injectable()
export class PostService {
    private postClient: ClientProxy;

    constructor(  ) {
        this.postClient = ClientProxyFactory.create({
          transport: Transport.TCP,
          options: {
              host: post_host,
              port: 8905
          }
        });
      }
    
    async create(createPostDto: CreatePostDto, owner: User): Promise<Post> {
        createPostDto.userId = owner._id;
        createPostDto.userName = owner.name;
        
        return this.postClient.send('create', createPostDto).toPromise();
    }

    async findAll(): Promise<Post[]> { // TO-DO data to represent limit 
        return this.postClient.send('findAll', 1).toPromise();
    }

    async update(id :string, userId: string, dto: CreatePostDto): Promise<Post> {
        const post = new UpdatePostDto(dto);
        post._id = id;

        return this.postClient.send('update', post).toPromise();
    }
     
    async delete(id: string, userId: string): Promise<Post> {
       return this.postClient.send('delete', { _id: id, userId }).toPromise();
    }
}
