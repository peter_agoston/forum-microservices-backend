import { Body, Controller, HttpStatus, Post as PostMethod, Res, UseGuards, Request, Get, Put, Param, Delete } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CreatePostDto } from '../shared/dtos/create-post.dto';
import { PostService } from './post.service';
import { Response } from 'express';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { Post } from '../shared/entities/post';

@UseGuards(JwtAuthGuard)
@ApiTags('post')
@ApiBearerAuth()
@Controller('post')
export class PostController {
    constructor(
        private postService: PostService
    ) {}

    @PostMethod()
    @ApiOperation({ summary: 'Create post' })
    @ApiResponse({ status: HttpStatus.CREATED, description: 'Created.' })
    async create(@Request() req, @Res() res: Response, @Body() dto: CreatePostDto) {
        
        this.postService.create(dto, req.user).then(post => {
            return res.status(HttpStatus.CREATED).send(post);
        })
        .catch(error => {
            return res.status(HttpStatus.BAD_REQUEST).send(JSON.stringify(error));
        })
    }

    @Get()
    @ApiOperation({ summary: 'List all posts.'})
    @ApiResponse({ status: HttpStatus.OK, type: [Post] })
    async findAll(): Promise<Post[]> {
        return this.postService.findAll();
    }

    @Put(':id')
    @ApiOperation({ summary: 'Update post.'})
    @ApiResponse({ status: HttpStatus.OK, type: [Post] })
    @ApiResponse({ status: HttpStatus.NOT_FOUND, description: 'Not found.' })
    async update(@Param('id') id: string, @Request() req, @Res() res: Response, @Body() dto: CreatePostDto): Promise<Post|any> {
        return this.postService.update(id, req.user._id, dto).then(post => {
            return res.status(HttpStatus.OK).send(post);
        })
        .catch(error => {
            return res.status(HttpStatus.NOT_FOUND).send(JSON.stringify(error));
        })
    }

    @Delete(':id')
    @ApiOperation({ summary: 'Delete post'})
    @ApiResponse({ status: HttpStatus.OK, description: 'Post deleted.' })
    @ApiResponse({ status: HttpStatus.NOT_FOUND, description: 'Not found.' })
    async delete(@Param('id') id: string, @Request() req, @Res() res: Response) {
        return this.postService.delete(id, req.user._id).then(post => {
            return res.status(HttpStatus.OK).send(post);
        })
        .catch(error => {
            return res.status(HttpStatus.NOT_FOUND).send(JSON.stringify(error));
        })
    }
}
