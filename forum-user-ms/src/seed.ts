import { Logger } from "@nestjs/common";
import { NestFactory } from "@nestjs/core";
import { SeedModule } from "./seed/seed.module";
import { Seeder } from "./seed/seeder";

const logger = new Logger('SeederMain');

async function bootstrap() {
    NestFactory.createApplicationContext(SeedModule)
      .then(appContext => {
        const seeder = appContext.get(Seeder);
        seeder
          .seed()
          .then(() => {
            logger.debug('Seeding complete!');
          })
          .catch(error => {
            logger.error('Seeding failed!');
            throw error;
          })
          .finally(() => appContext.close());
      })
      .catch(error => {
        throw error;
      });
  }
  bootstrap();