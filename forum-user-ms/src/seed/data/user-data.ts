import { CreateUserDto } from "src/user/dtos/create-user.dto";

export const userSeedData: CreateUserDto[] = [
    {
        "name": "Zalan",
        "email": "zalan@gmail.com",
        "password": "1234"
    },
    {
        "name": "Peter",
        "email": "peter@gmail.com",
        "password": "1234"
    },
    {
        "name": "Gabor",
        "email": "gabor@gmail.com",
        "password": "1234"
    } 
]