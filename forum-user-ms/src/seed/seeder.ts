import { Injectable, Logger } from "@nestjs/common";
import { RpcException } from "@nestjs/microservices";
import { CreateUserDto } from "src/user/dtos/create-user.dto";
import { UserService } from "src/user/user.service";
import { userSeedData } from "./data/user-data";

@Injectable()
export class Seeder {
    logger;

    constructor(private readonly userService: UserService) {
        this.logger = new Logger('Seeder');
    }

    async seed() {
        await this.users()
        .then(completed => {
            this.logger.debug('Successfuly completed seeding users...');
            Promise.resolve(completed);
        })
        .catch(error => {
            this.logger.error('Failed seeding users...');
            Promise.reject(error);
        });
    }

    async users() {
        const users = userSeedData;

        return await Promise.all(
            users.map((user: CreateUserDto) => {
                return this.userService.create(user)
                    .then(() => {
                        return Promise.resolve(true);
                    })
                    .catch((err: RpcException) => {
                        return Promise.resolve(true);
                    })
            })
        )
        .then(() => {
            return Promise.resolve(true);
        })
        .catch(err => {
            return Promise.reject(err);
        })
    }
}