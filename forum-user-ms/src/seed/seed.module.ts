import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { db_host, db_name } from 'src/config';
import { UserModule } from 'src/user/user.module';
import { Seeder } from './seeder';

@Module({
  imports: [
    MongooseModule.forRoot(`mongodb://${db_host}/${db_name}`),
    UserModule],
  providers: [Seeder]
})
export class SeedModule {}
