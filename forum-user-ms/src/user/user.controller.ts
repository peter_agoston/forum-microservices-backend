import { CreateUserDto } from './dtos/create-user.dto';
import { UserService } from './user.service';
import { Controller } from '@nestjs/common';
import { User } from './schemas/user.schema';
import { MessagePattern } from '@nestjs/microservices';

@Controller('user')
export class UserController {
    constructor(private readonly userService: UserService) {}

    @MessagePattern('findAll')
    async findAll(limit: number): Promise<User[]> {
        return this.userService.findAll();
    }

    @MessagePattern('findById')
    async findById(id: string): Promise<User> {
        return this.userService.findById(id);
    }

    @MessagePattern('findByEmail')
    async findByEmail(email: string): Promise<User> {
        return this.userService.findByEmail(email);
    }

    @MessagePattern('create')
    async create(dto: CreateUserDto): Promise<User> {
        return this.userService.create(dto);
    }

    @MessagePattern('emailingList')
    async retrieveEmailingList(): Promise<User[]> {
        return this.userService.retrieveEmailingList();
    }
}
