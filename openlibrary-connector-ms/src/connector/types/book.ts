export class Book {
    title: string;
    isbn: string;
    author: string;
    goodReadsId: string;
}
