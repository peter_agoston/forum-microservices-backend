export class OpenLibraryBook {
    title: string;
    isbn_13: string;
    authors: any[];
    identifiers: {
        goodreads: string
    };
}
