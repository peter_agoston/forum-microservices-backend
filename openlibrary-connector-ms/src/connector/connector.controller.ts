import { ConnectorService } from './connector.service';
import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { Book } from './types/book';

@Controller('connector')
export class ConnectorController {
    constructor(
        private connectorService: ConnectorService
    ) {}

    @MessagePattern('getBookInfo')
    async getBookInfo(isbn: string): Promise<any> {
        return this.connectorService.getBookInfoByIsbn(isbn);
    }
}
