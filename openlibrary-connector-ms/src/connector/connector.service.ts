import { open_library_url } from './../config';
import { OpenLibraryBook } from './types/openlibrary-book';
import { Injectable, HttpService, Logger } from '@nestjs/common';
import { Book } from './types/book';
import { RpcException } from '@nestjs/microservices';

const logger = new Logger('OpenLibrary Connector');

@Injectable()
export class ConnectorService {
    constructor(
        private httpService: HttpService
    ) {}

    async getBookInfoByIsbn(isbn: string): Promise<Book> {
        return this.httpService.get(`${open_library_url}/isbn/${isbn}.json`).toPromise()
            .then(response =>{
                return this.responseToBookDto(response.data).then(book => {
                    if (!book.isbn) {
                        book.isbn = isbn;
                    }
                    return book;
                }).catch(err => {
                    logger.error(err.message);
                    throw new RpcException('Document not found.');
                });
            })
            .catch(err => {
                logger.error(err.message);
                throw new RpcException('Document not found.');
            })
    }

    async getAuthorInfo(authorId: string): Promise<any> {
        return this.httpService.get(`${open_library_url}/${authorId}.json`).toPromise()
            .then(response => {
                return response.data.name;
            })
            .catch(() => {
                return '';
            });
    }

    async getMultipleAuthorInfo(ids: any[], index: number): Promise<any> {
        return this.getAuthorInfo(ids[index].key)
            .then(name => {
                    if (ids[index+1]) {
                        return this.getMultipleAuthorInfo(ids, index+1)
                            .then(name2 => {
                                return name + ', ' + name2;
                            });
                    }
                    return name;
                } 
            )
    }

    // TO-DO: move method to separate service
    async responseToBookDto(data: OpenLibraryBook): Promise<Book> {
        const book = new Book();
        if (data.isbn_13) {
            book.isbn = data.isbn_13[0];
        }
        if (data.identifiers && data.identifiers.goodreads) {
            book.goodReadsId = data.identifiers.goodreads[0];
        }
        book.title = data.title;

        if (data.authors) {
            return this.getMultipleAuthorInfo(data.authors, 0)
                .then(author => {
                    book.author = author;
                    return book;
                });
        } else {
            return book;
        }
    } 
}
