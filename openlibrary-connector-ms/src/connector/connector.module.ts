import { HttpModule, Module } from '@nestjs/common';
import { ConnectorController } from './connector.controller';
import { ConnectorService } from './connector.service';

@Module({
  imports: [HttpModule],
  controllers: [ConnectorController],
  providers: [ConnectorService]
})
export class ConnectorModule {}
