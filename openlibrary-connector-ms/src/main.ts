import { Transport } from '@nestjs/microservices';
import { library_host } from './config';
import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

const logger = new Logger('Main');

const microserviceOptions = {
  transport: Transport.TCP,
  options: {
    retryAttempts: 3,
    retryDelay: 2000,
    host: library_host,
    port: 8885,
  },
};

async function bootstrap() {
  const app = await NestFactory.createMicroservice(AppModule, microserviceOptions);
  await app.listen(() => {
    logger.log('OpenLibrary connector microservice is listening...');
  });
}
bootstrap();
