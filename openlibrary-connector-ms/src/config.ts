export const library_host = process.env.LIBRARY_HOST || '127.0.0.1';
export const open_library_url = process.env.OPEN_LIBRARY_URL || 'https://openlibrary.org';