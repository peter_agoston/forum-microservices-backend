import { Module } from '@nestjs/common';
import { BookModule } from './book/book.module';
import { db_host, db_name } from './config';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forRoot(`mongodb://${db_host}/${db_name}`),
    BookModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
