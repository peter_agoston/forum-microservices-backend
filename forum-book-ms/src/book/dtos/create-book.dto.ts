export class CreateBookDto {
    title: string;
    isbn: string;
    author: string;
    goodReadsId: string;
}
