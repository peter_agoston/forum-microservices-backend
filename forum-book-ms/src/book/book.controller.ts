import { BookService } from './book.service';
import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { Book } from './schemas/book.schema';
import { CreateBookDto } from './dtos/create-book.dto';

@Controller('book')
export class BookController {
    constructor( private readonly bookService: BookService ) {};

    @MessagePattern('findAll')
    async findAll(limit: number): Promise<Book[]> {
        return this.bookService.findAll();
    }

    @MessagePattern('findById')
    async findById(id: string): Promise<Book> {
        return this.bookService.findById(id);
    }

    @MessagePattern('findByEmail')
    async findByEmail(isbn: string): Promise<Book> {
        return this.bookService.findByIsbn(isbn);
    }

    @MessagePattern('create')
    async create(dto: CreateBookDto): Promise<Book> {
        return this.bookService.create(dto);
    }
}
