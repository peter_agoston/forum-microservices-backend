import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Book, BookDocument } from './schemas/book.schema';
import { Model } from 'mongoose';
import { CreateBookDto } from './dtos/create-book.dto';

@Injectable()
export class BookService {

    constructor(@InjectModel(Book.name) private bookModel: Model<BookDocument>) {}

    async create(createBookDto: CreateBookDto): Promise<Book> {
        const query = createBookDto.isbn ? 
            { isbn: createBookDto.isbn } 
            : 
            { title: createBookDto.title, author: createBookDto.author };

        const update = { $setOnInsert: createBookDto };
        const options = { upsert: true, new: true, omitUndefined: true, useFindAndModify: false };

        // create document only if the ISBN (or author-title pair) is not already present in the collection
        return this.bookModel.findOneAndUpdate(query, update, options).exec();
    }

    async findById(id: string): Promise<Book | undefined> {
        return this.bookModel.findById(id).exec();
    }

    async findByIsbn(isbn: string): Promise<Book | undefined> {
        return this.bookModel.findOne({ isbn: isbn}).exec();
    }

    async findByTitle(title: string): Promise<Book[]> {
        return this.bookModel.find({ title: title}).exec();
    }

    async findAll(): Promise<Book[]> {
        return this.bookModel.find().exec();
      }
}
