import { book_host, db_host, db_name } from './config';
import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';

const logger = new Logger('Main');

const microserviceOptions = {
  transport: Transport.TCP,
  options: {
    retryAttempts: 3,
    retryDelay: 2000,
    host: book_host,
    port: 8895,
  },
};

async function bootstrap() {
  const app = await NestFactory.createMicroservice(AppModule, microserviceOptions);
  await app.listen(() => {
    logger.log(`connect to: ${db_host}/ ${db_name}`);
    logger.log('Book microservice is listening...');
  });
}
bootstrap();
