import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type SubscriptionDocument = Subscription & Document;

@Schema()
class SubKey {
  @Prop()
  p256dh: string;
  
  @Prop()
  auth: string;
}

@Schema()
export class Subscription {
  @Prop({ required: true })
  endpoint: string;

  @Prop({ required: true, type: SubKey })
  keys: {
    p256dh: string,
    auth: string
  };

  @Prop({ required: true })
  userId: string;
}

export const SubscriptionSchema = SchemaFactory.createForClass(Subscription);