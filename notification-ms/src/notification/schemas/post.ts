import { Book } from "./book";

export class Post {
  _id: string;
  userId: string;
  content: string;
  quote: string; 
  book: Book;
  date: Date;
}
