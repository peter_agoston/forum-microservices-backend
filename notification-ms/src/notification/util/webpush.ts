import { vapid_private_key, vapid_public_key } from '../../config';
import { PushSubscription, sendNotification, setVapidDetails } from 'web-push';
import { WPSubscription } from './webpush-subscription';

export class WebPush {
  static configure() {
    setVapidDetails(
      'mailto:test@test.com',
      vapid_public_key,
      vapid_private_key
    );
  }
  
  static send(subscription: WPSubscription, payload: string | Buffer) {
    return sendNotification(subscription, payload);
  }
}