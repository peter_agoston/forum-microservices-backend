import { PushSubscription } from 'web-push';
import { Subscription } from '../schemas/subscription.schema';

export class WPSubscription implements PushSubscription {
    constructor(dbModel: Subscription) {
        this.endpoint = dbModel.endpoint;
        this.keys = dbModel.keys;
        this.userId = dbModel.userId;
    }

    endpoint: string;
    keys: {
        p256dh: string;
        auth: string;
    };
    userId: string;
}