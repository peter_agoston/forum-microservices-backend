import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Subscription, SubscriptionDocument } from './schemas/subscription.schema';
import { WebPush } from './util/webpush';
import { WPSubscription } from './util/webpush-subscription';

@Injectable()
export class WebNotificationService {

    constructor(@InjectModel(Subscription.name) private subscriptionModel: Model<SubscriptionDocument>) {}
    
    async register(subscription: WPSubscription): Promise<any> {
        const createSubscription = new this.subscriptionModel(subscription);

        return createSubscription.save()
    }

    async notifyOthers(emitterUserId: string, title: string, date?: Date, message?: string) {
        const subs = await this.subscriptionModel.find({userId: {$ne: emitterUserId}});
        
        const payload = JSON.stringify({notification: { 
                    title,
                    message,
                    originUserID: emitterUserId,
                    timestamp: date
                }});
                
        try {
            subs.forEach(sub => {
                WebPush.send(sub, payload)
                    .catch(err => {
                        if (err.statusCode && err.statusCode == 410) {
                            console.log(sub);
                            this.subscriptionModel.findByIdAndDelete(sub._id)
                        }    
                    });
            })

            return subs;
        } 
        catch (err) {
            console.error(err);
            return err;
        }
    }

    async unregister(id: string): Promise<Subscription> {
        return this.subscriptionModel.findByIdAndDelete(id);
    }
}
