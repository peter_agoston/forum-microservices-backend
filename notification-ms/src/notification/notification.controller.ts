import { Controller } from '@nestjs/common';
import { EventPattern, MessagePattern } from '@nestjs/microservices';
import { CreatePostEvent } from './events/create-post.event';
import { Subscription } from './schemas/subscription.schema';
import { WebNotificationService } from './web-notification.service';

@Controller('notification')
export class NotificationController {
    constructor( private readonly webNotificationService: WebNotificationService ) {}

    @MessagePattern('register')
    async register(subscription: any) : Promise<Subscription> {
        return this.webNotificationService.register(subscription);
    }

    @MessagePattern('unregister')
    async unregister(id: string) : Promise<Subscription> {
        return this.webNotificationService.unregister(id);
    }

    @EventPattern('post_created')
    async handlePostCreated(postEvent: CreatePostEvent) { // there is no need for the whole data classes! may be it is
        this.webNotificationService.notifyOthers(postEvent.userId, `New post by ${postEvent.ownerDisplayName}!`, postEvent.date)
    }
}
