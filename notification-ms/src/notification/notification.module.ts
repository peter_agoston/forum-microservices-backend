import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { NotificationController } from './notification.controller';
import { SubscriptionSchema } from './schemas/subscription.schema';
import { WebNotificationService } from './web-notification.service';

@Module({
  imports: [
    MongooseModule.forFeature([{name: 'Subscription', schema: SubscriptionSchema}])
  ],
  controllers: [NotificationController],
  providers: [WebNotificationService]
})
export class NotificationModule {}
