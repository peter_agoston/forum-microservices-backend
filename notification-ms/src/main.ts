import { noty_host } from './config';
import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';
import { WebPush } from './notification/util/webpush';

const logger = new Logger('Main');

const microserviceOptions = {
  transport: Transport.TCP,
  options: {
    retryAttempts: 3,
    retryDelay: 2000,
    host: noty_host,
    port: 9000,
  },
};

async function bootstrap() {
  const app = await NestFactory.createMicroservice(AppModule, microserviceOptions);
  await app.listen(() => {
    logger.log('Notify microservice is listening...');
  });

  WebPush.configure();
}

bootstrap();
