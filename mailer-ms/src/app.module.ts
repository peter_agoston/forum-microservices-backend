import { Module } from '@nestjs/common';
import { EmailerModule } from './mailer/emailer.module';
import { MailerModule } from '@nestjs-modules/mailer';

@Module({
  imports: [
    EmailerModule,
    MailerModule.forRoot({
      transport: {
        host: "smtp.mailtrap.io",
        port: 2525,
        auth: {
          user: "3074c5eb8e3efc",
          pass: "598792b01c523d"
        }
      },
      defaults: {
        from:'"4UM" <noreply@4um.com>'
      }
    })]
})
export class AppModule {}
