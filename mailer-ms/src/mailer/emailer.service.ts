import { Injectable, Logger } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { CreatePostEvent } from './events/create-post.event';
import { ClientProxy, ClientProxyFactory, Transport } from '@nestjs/microservices';
import { user_host } from 'src/config';
import { User } from './schemas/user';

@Injectable()
export class EmailerService {
    private userClient: ClientProxy;
    private logger: Logger;

    constructor(private readonly mailer: MailerService) {
        this.userClient = ClientProxyFactory.create({
            transport: Transport.TCP,
            options: {
                host: user_host,
                port: 8875
            }
          });

        this.logger = new Logger('Mailing Service');
    }

    async deliverPostByMail(postEvent: CreatePostEvent) {

        this.userClient.send('emailingList', 0).toPromise().then((emailingList: User[]) => {

            const emailList = emailingList
            .filter((user: User) => {
                return user._id != postEvent.userId
            })
            .map((user: User) => {
                return user.email;
            });

            if (emailList && emailList.length > 0) {
                this.mailer.sendMail({
                    to: emailList,
                    from: 'noreply@4um.com',
                    subject: `New post from ${postEvent.ownerDisplayName}!`, 
                    text: `${postEvent.content}\n
                        \n${postEvent.quote ? postEvent.quote : ''}\n
                        - ${postEvent.book?.author ? postEvent.book?.author : ''}`
                })
                .then(() => { this.logger.log('Messages delivered.') })
                .catch(err => { this.logger.error('Message delivery failed: ' + err) })
            }
            
        }).catch(err => { this.logger.error(err) });
    }
}
