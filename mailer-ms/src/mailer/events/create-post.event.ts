import { Post } from "../schemas/post";

export class CreatePostEvent extends Post {
    constructor(post?: Post) {
        super();

        Object.assign(this, post);
    }

    ownerDisplayName: string;
}