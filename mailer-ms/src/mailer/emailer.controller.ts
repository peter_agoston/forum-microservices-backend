import { Controller } from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
import { CreatePostEvent } from './events/create-post.event';
import { EmailerService } from './emailer.service';

@Controller('mailer')
export class EmailerController {
    constructor(private readonly mailerService: EmailerService) {}

    @EventPattern('post_created')
    async handlePostCreated(postEvent: CreatePostEvent) {
        this.mailerService.deliverPostByMail(postEvent);
    }
}
