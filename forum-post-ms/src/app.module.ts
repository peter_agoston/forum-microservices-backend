import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { db_host, db_name } from './config';
import { PostModule } from './post/post.module';

@Module({
  imports: [
    MongooseModule.forRoot(`mongodb://${db_host}/${db_name}`),
    PostModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
