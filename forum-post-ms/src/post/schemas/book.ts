export class Book {
    _id: string;
    title: string;
    isbn: string;
    author: string;    
    goodReadsId: string;
}
