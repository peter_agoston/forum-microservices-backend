import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Book } from './book';

export type PostDocument = Post & Document;

@Schema()
export class Post {
  @Prop({ required: true })
  userId: string;

  @Prop()
  content: string;

  @Prop()
  quote: string; 

  @Prop()
  book: Book;

  @Prop({ required: true })
  date: Date;
}

export const PostSchema = SchemaFactory.createForClass(Post);