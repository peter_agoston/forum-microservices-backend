import { Injectable } from '@nestjs/common';
import { ClientProxy, ClientProxyFactory, RpcException, Transport } from '@nestjs/microservices';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { book_host } from 'src/config';
import { Book } from './schemas/book';
import { CreatePostDto } from './dto/create-post.dto';
import { PostDto } from './dto/post.dto';
import { PostDocument, Post } from './schemas/post.schema';

@Injectable()
export class PostService {
    private bookClient: ClientProxy;

    constructor(@InjectModel(Post.name) private postModel: Model<PostDocument>) {
        this.bookClient = ClientProxyFactory.create({
            transport: Transport.TCP,
            options: {
                host: book_host,
                port: 8895
            }
        });
    }

    async create(createPostDto : CreatePostDto) : Promise<Post> {
        const createPost = new this.postModel(createPostDto);
        createPost.date = new Date();
             
        if (createPostDto.bookId) {
            return this.bookClient.send('findById', createPostDto.bookId).toPromise()
            .then((book: Book) => {
                createPost.book = book;
                return createPost.save();
            })
            .catch(() => {
                createPost.book = new Book();
                createPost.book._id = createPostDto.bookId;
                return createPost.save();
            });
        } else {
            return createPost.save();
        }
    }

    async findAll() : Promise<Post[]> {
        return this.postModel.find().sort({date: 'desc'});
    }

    async update(postDto : PostDto) : Promise<Post> {
        const query = {_id: postDto._id, userId: postDto.userId};
        const post = new this.postModel(postDto);
        post.date = new Date();
                
        if (postDto.bookId) {
            return this.bookClient.send('findById', postDto.bookId).toPromise()
                .then((book: Book) => {
                    post.book = book;
                    return post;
                })
                .catch(() => {
                    post.book = new Book();
                    post.book._id = postDto.bookId;
                    return post;
                })
                .finally(() => {
                    return this.postModel.findOneAndUpdate(query, post, { new: true, useFindAndModify: false }).exec()
                        .then(post => { 
                            if (!post) {
                                throw new RpcException('Document not found.');    
                            }
                            return post; 
                        });
                });
        } else {
            return this.postModel.findOneAndUpdate(query, post, { new: true, useFindAndModify: false }).exec()
            .then(post => { 
                if (!post) {
                    throw new RpcException('Document not found.');    
                }
                return post; 
            });
        }
    }

    async delete(id: string, userId: string) : Promise<Post> {
        const query = {_id: id, userId };
    
        return this.postModel.findOneAndDelete(query).exec()
            .then(post => { 
                if (!post) {
                    throw new RpcException('Document not found.');    
                }
                return post; 
            });
    }
}
