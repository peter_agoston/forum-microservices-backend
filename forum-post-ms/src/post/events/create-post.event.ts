import { Post } from "../schemas/post.schema";

export class CreatePostEvent extends Post {
    constructor(post?: Post) {
        super();

        this.userId = post.userId;
        this.book = post.book;
        this.content = post.content;
        this.date = post.date;
        this.quote = post.quote;
    }

    ownerDisplayName: string;
}