import { Book } from "../schemas/book";

export class PostDto {
    _id: string;
    userId: string;
    content: string;
    quote: string; 
    bookId: string;
}