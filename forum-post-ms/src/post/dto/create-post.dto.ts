export class CreatePostDto {
  userId: string;
  userName: string;
  content: string;
  quote: string; 
  bookId: string;
}