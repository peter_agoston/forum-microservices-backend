import { Controller } from '@nestjs/common';
import { ClientProxy, ClientProxyFactory, MessagePattern, Transport } from '@nestjs/microservices';
import { mail_host, noty_host } from 'src/config';
import { CreatePostDto } from './dto/create-post.dto';
import { PostDto } from './dto/post.dto';
import { CreatePostEvent } from './events/create-post.event';
import { PostService } from './post.service';
import { Post } from './schemas/post.schema';

@Controller('post')
export class PostController {
    private notyClient: ClientProxy;
    private mailClient: ClientProxy;

    constructor( private readonly postService: PostService ) {
        this.notyClient = ClientProxyFactory.create({
            transport: Transport.TCP,
            options: {
                host: noty_host,
                port: 9000
            }
          });
        
        this.mailClient = ClientProxyFactory.create({
            transport: Transport.TCP,
            options: {
                host: mail_host,
                port: 9010
            }
          });
    };

    @MessagePattern('findAll')
    async findall(limit: number): Promise<Post[]> {
        return this.postService.findAll();
    }

    @MessagePattern('create')
    async create(createPostDto: CreatePostDto): Promise<Post> {
        return this.postService.create(createPostDto)
            .then((post: Post) => {
                // notify other users
                const event = new CreatePostEvent(post);
                event.ownerDisplayName = createPostDto.userName;
                
                this.notyClient.emit('post_created', event);
                this.mailClient.emit('post_created', event);
                
                return post;
            });
    }

    @MessagePattern('update')
    async update(postDto: PostDto): Promise<Post> {
        return this.postService.update(postDto);
    }

    @MessagePattern('delete')
    async delete(postDto: PostDto): Promise<Post> {
        return this.postService.delete(postDto._id, postDto.userId);
    }
}
