# ForUm Microservices Backend

A group-based blog project using a microservice-oriented back-end architecture.

# System diagram

```mermaid
graph TD
linkStyle default interpolate basis
    client([Client])-.-posts[posts a quote]-.->forum(4UM Web Application)
    forum-.->emailing(Email Service)
    emailing-.- sends -.->others([Other Clients])
    forum-.->notif(Notification Service)
    notif-.- sends -.->others([Other Clients])
    
    client2([Client])-.-enters[enters ISBN]-.->forum2(4UM Web Application)
    forum2-.- resp[responds with book information] -.-> client2
    forum2-.->lib(Third-party Book Database)
    lib-.->forum2
    
```
# Container diagram of 4UM

```mermaid
graph TD
    WebApp-->Forum-API
    WebServer-->WebApp
    subgraph Back-end Network
        Forum-API --> User-MS & Book-MS & Post-MS
        mongo[(MongoDB)]
        User-MS & Book-MS & Post-MS --> mongo
        subgraph Data Services
            Book-MS & User-MS & Post-MS
        end
        Post-MS -->Notification-MS
        Post-MS -->Mailer-MS
        Post-MS -->Book-MS
        Mailer-MS -->User-MS
        Notification-MS-->mongo
        subgraph Notification Services
            Mailer-MS & Notification-MS
        end
        Forum-API-->OpenLibrary-Connector-MS
        subgraph Third-party Services
            OpenLibrary-Connector-MS
        end
    end
```

# Component diagram of Forum-API

```mermaid
graph LR
linkStyle default interpolate basis
    Client--> AuthController & UserController & BookController & PostController & NotifyController
    
    subgraph API
        AuthController-->AuthService
        UserController
        BookController
        PostController
        NotifyController
    end
    
    subgraph _
        subgraph User-MS
            UserService
        end
        subgraph Book-MS
            BookService
        end
        subgraph Post-MS
            PostService
        end
        subgraph OpenLibrary-Connector-MS
            LibraryService
        end
        subgraph Notification-MS
            NotificationService
        end
        AuthService --> UserService
        UserController --> UserService
        BookController --> BookService & LibraryService
        PostController --> PostService
        NotifyController --> NotificationService
    end
```
Note: the above diagram omits the Service classes of Forum-API as well as the Controller classes of the microservices. The role of these classes is to handout each task to the corresponding service.

# Component diagram of Post-MS and dependent services

```mermaid
graph TD
linkStyle default interpolate basis
    subgraph Post-MS
        PostController --> PostService
    end
    subgraph Book-MS
        BookController --> BookService
    end
    subgraph Notification-MS
        NotificationController --> WebNotificationService
    end
    subgraph Mailer-MS
        EmailerController --> EmailerService
    end
    PostService --> BookController
    PostService --post_created--> NotificationController & EmailerController
    PostService & BookService & WebNotificationService --> mongo[(MongoDB)]
```

# Component diagram of User-MS

```mermaid
graph TD
linkStyle default interpolate basis
    subgraph User-MS
        UserController & Seeder--> UserService
    end
    UserService --> mongo[(MongoDB)]

    subgraph OpenLibrary-Connector-MS
        ConnectorController --> ConnectorService
    end
    ConnectorService --> openLib([OpenLibrary API])
```

# Sequence diagram of creating a new post

Prior to saving a new post, the referenced book is saved if not already present in the database.

```mermaid
sequenceDiagram
    participant Client
    participant API
    participant book_ms as Book-MS

    Client->>API: POST /book
    API->>book_ms: MessagePattern: 'create'
    book_ms->>+MongoDB: findOneAndUpdate() - upsert=true
    Note over book_ms, MongoDB: Insert only if the ISBN (or author-title pair)
    Note over book_ms, MongoDB: is not already present.
    MongoDB-->>-book_ms: Book
    book_ms-->>API: Book
    API-->>Client: HTTP 201
```    
Upon saving a new post by a user, first the corresponding Book entry is retrieved from the Book-Microservice. (The incoming data transfer object contains only the ID of the referenced book.)
Next the Post object (embedding the Book object) is to be inserted into the database. If the operation succeeds, the Notification- and Mailer-Microservices will be notified to anounce other users via WebPush Notifications or email.   
In this case the Notification-Microservice will access the database to gather the notification subscriptions, and the Mailer-Microservice will request the list of user emails from the User-Microservice.

```mermaid
sequenceDiagram
    participant Client
    participant API
    participant post_ms as Post-MS
    participant book_ms as Book-MS
    participant user_ms as User-MS
    participant MongoDB
    participant noty_ms as Notification-MS 
    participant mail_ms as Mailer-MS
    participant users as Other Users

    Client->>API: POST /post
    API->>post_ms: MessagePattern: 'create'
    post_ms->>book_ms: MessagePattern: 'findById'
    book_ms->>+MongoDB: findById()
    MongoDB-->>-book_ms: Book
    book_ms-->>post_ms: Book
    post_ms->>+MongoDB: save()
    MongoDB-->>-post_ms: Post
    
    post_ms->>noty_ms: EventPattern: 'post_created'
    post_ms->>mail_ms: EventPattern: 'post_created'

    post_ms-->>API: Post
    API-->>Client: HTTP 201

    noty_ms->>+MongoDB: find()
    MongoDB-->>-noty_ms: Subscription[]
    noty_ms->>users: Push Notification: "New post!"

    mail_ms->>user_ms: MessagePattern: 'emailingList'
    user_ms->>+MongoDB: find().select('_id, email')
    MongoDB-->>-user_ms: User[]
    user_ms-->>mail_ms: User[]
    mail_ms->>users: SMTP: "New post!"

```

# Data classes
```mermaid
classDiagram
    class Entity { 
        _id: string
    }

    class Book {
        title: string
        isbn: string
        author: string
        goodReadsId: string
    }

    class User {
        name: string
        email: string
        password: string
    }

    class Post {
        userId: string
        content: string
        quote: string
        book: Book
        date: Date
    }

    Entity <|-- Book
    Entity <|-- Post
    Entity <|-- User 
```

# Starting the project
To build the project run:

```console
$ docker-compose build
```

To run the project run:
```console
$ docker-compose up
```
